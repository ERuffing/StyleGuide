Style Guide
=============
Copyright &copy; 2022 by Ethan Ruffing.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">The Style Guide</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Ethan Ruffing</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.

This is intended to be a programming style guide for use with a variety of
programming languages. Most of it is suitable for any language, but there are
language-specific sections included as necessary.

The latest releases of the style guide can be found [here](https://gitlab.com/ERuffing/StyleGuide/-/releases).
